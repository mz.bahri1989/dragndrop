namespace App {

    export class FormRenderer extends BaseComponent<HTMLFormElement> {
        titleElement: HTMLInputElement;
        descElement: HTMLTextAreaElement;
        peopleElement: HTMLInputElement;

        constructor() {
            super('project-input', 'user-input');

            this.titleElement = <HTMLInputElement>this.element.querySelector('#title')!;
            this.descElement = <HTMLTextAreaElement>this.element.querySelector('#description')!;
            this.peopleElement = <HTMLInputElement>this.element.querySelector('#people')!;
        }


        private validateInput(): boolean {
            const title = this.titleElement.value;
            const desc = this.descElement.value;
            const people = this.peopleElement.value;

            return (title.length !== 0 && desc.length !== 0 && people.length !== 0)


        }

        clearForm() {
            this.titleElement.value = '';
            this.descElement.value = '';
            this.peopleElement.value = '';
        }

        @binder
        private submitForm(e: Event) {
            e.preventDefault();
            if (this.validateInput()) {
                const project = {
                    id: Date.now(),
                    title:this.titleElement.value,
                    description:this.descElement.value,
                    people:parseInt(this.peopleElement.value),
                    active: true};
                projectState.addProject(project);
                db.addRecord(project, 'myStore').then(() => {
                    this.clearForm();
                });

                //TODO: error handling
            }


        }

        setUp() {
            this.element.addEventListener('submit', this.submitForm.bind(this));
        }

    }
}



