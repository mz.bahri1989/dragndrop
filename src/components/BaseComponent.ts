namespace App {
    export abstract class BaseComponent<T extends HTMLElement> {
        renderElement: HTMLTemplateElement;
        element: T;

        protected constructor(templateId: string,elementId:string) {
            this.renderElement = <HTMLTemplateElement>document.getElementById(templateId)!;
            const node = document.importNode(this.renderElement.content, true);
            this.element = <T>node.firstElementChild;
            this.element.id = elementId;
            this.setUp();
            this.attach(this.element)
        }

        attach(element: HTMLElement) {
            const hostElement = <HTMLDivElement>document.getElementById('app')!;
            hostElement.insertAdjacentElement('beforeend', element)
        }

        abstract setUp(): void;


    }
}