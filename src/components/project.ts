namespace App {

    export class Project {
        id: number;
        title: string;
        description: string;
        people: number;
        active: boolean;

        constructor(props: ProjectType) {
            const {id, title, description, people, active} = props;
            this.id = id;
            this.title = title;
            this.description = description;
            this.people = people;
            this.active = active;
        }

        @binder
        private deleteProject(_: Event) {
            db.deleteRecord(this.id, 'myStore').then(() => {
                projectState.deleteProject(this.id)
            })

        }

        @binder
        private dragStart(e: DragEvent) {
            e.dataTransfer!.setData('text/plain',this.id.toString())
        }

        @binder
        private dragEnd(_: DragEvent) {
           // console.log(e)
        }



        private attachEventListeners(element: HTMLElement, eventList: EventType[]) {
            eventList.forEach((eventItem) => {
                element.addEventListener(eventItem.name, <EventListenerOrEventListenerObject>eventItem.method)
            })

        }

        private attachOptionBtn(text: string, className: string, eventList: EventType[], parent: HTMLElement) {
            const btn = document.createElement('button');
            btn.innerHTML = text;
            btn.classList.add(className);
            this.attachEventListeners(btn, eventList);
            parent.appendChild(btn);
        }

        renderProject(parent: HTMLElement, wrapper: string) {
            const wrapElement = document.createElement(wrapper);
            wrapElement.setAttribute('draggable','true');
            wrapElement.innerHTML = `<p>${this.title}</p><p>${this.description}</p><p>${this.people}</p>`;
            const wrapElementEvents = [
                {name: 'dragstart', method: this.dragStart},
                {name: 'dragend', method: this.dragEnd}
            ];
            this.attachEventListeners(wrapElement, wrapElementEvents);

            this.attachOptionBtn('delete', 'remove', [{
                name: 'click',
                method: this.deleteProject
            }], wrapElement);

            parent.appendChild(wrapElement);
        }
    }
}