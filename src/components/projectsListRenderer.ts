namespace App {
    export class ProjectsListRenderer extends BaseComponent<HTMLElement> {
        ulElement: HTMLUListElement;
        activeStatus: boolean;
        projects: ProjectType[] = [];

        constructor(type: 'active' | 'finished') {
            super('project-list', `${type}-projects`);
            this.activeStatus = type === 'active';
            this.ulElement = <HTMLUListElement>this.element.querySelector('ul')!;
            this.renderContent();
        }

        setUp() {
            projectState.addListener((projects: ProjectType[]) => {
                this.projects = projects.filter(item => item.active === this.activeStatus);
                this.renderProjects();
            });
        }

        @binder
        dragOver(e: DragEvent) {
            if(e.dataTransfer && e.dataTransfer.types[0] === 'text/plain'){
                e.preventDefault();
                this.ulElement.classList.add('droppable')
            }

        }

        @binder
        dropHandler(e: DragEvent) {
            const t = e.dataTransfer!.getData('text/plain');
            projectState.moveProject(t,this.activeStatus)


        }

        @binder
        dragLeave(_: DragEvent) {
            this.ulElement.classList.remove('droppable')
        }


        private attachEventListeners(eventList: EventType[]) {
            eventList.forEach((eventItem) => {
                this.ulElement.addEventListener(eventItem.name,<EventListenerOrEventListenerObject> eventItem.method)
            })

        }

        private renderListItem(item: ProjectType) {
            new Project(item).renderProject(this.ulElement, 'li')
        }


        private renderProjects() {
            this.ulElement.innerHTML = '';
            this.projects.forEach(project => {
                this.renderListItem(project);
            })
        }

        renderContent() {
            (this.element.querySelector('h2')!).innerText = `${this.activeStatus ? 'Active':'Finished'} Projects`;
            this.attachEventListeners([
                {name:'dragover',method:this.dragOver},
                {name:'dragleave',method:this.dragLeave},
                {name:'drop',method:this.dropHandler}
            ]);

        }
    }
}

