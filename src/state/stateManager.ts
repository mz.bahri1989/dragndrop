
namespace App {
    export class StateManager{
        private listeners: Function[] = [];
        private projectList: ProjectType[] =[];
        private static instance: StateManager;
        private constructor(){
            this.getData()
        }
        static getInstance(){
            if(this.instance){
                return this.instance;
            }
            this.instance = new StateManager();
            return this.instance;
        }
        get list(){
            return this.projectList.slice();
        }
        getData(){
            db.getAll<ProjectType[]>('myStore').then((res:ProjectType[]) => {
                this.projectList = res;
                this.listeners.forEach(listener => listener(this.list));

            });
        }
        @binder
        updateListeners(){
            this.listeners.forEach(listener => listener(this.list));
        }
        addListener(listener: Function){
            this.listeners.push(listener);
        }
        addProject(project: ProjectType){
            this.projectList.push(project);
           this.updateListeners()
        }
        deleteProject(id:number){
            const x = this.projectList.findIndex(item => id === item.id);
            this.projectList.splice(x,1);
            this.updateListeners()
        }
        moveProject(t: string, status: boolean){
            const movingIndex = this.projectList.findIndex(item => item.id.toString() === t);
            const oldStatus = this.projectList[movingIndex].active;
            if(oldStatus !== status){
                this.projectList[movingIndex].active = status;
                const item = this.projectList[movingIndex];
                db.updateRecord(item,'myStore')
                    .then(()=>  this.updateListeners())
            }


        }
        editProject(){}
    }
    export const projectState = StateManager.getInstance();
}