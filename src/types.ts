namespace App {
    export type ProjectType = {
        id: number;
        title: string;
        description: string;
        people: number;
        active: boolean;
    }
    export type EventType = {
        name: string;
        method: any
    }
}