namespace App {
    // binder decorator to bind "this" to methods
    export function binder(_: any, _2: string, descriptor: PropertyDescriptor) {
        const originnalMethod = descriptor.value;
        const adjDescriptor: PropertyDescriptor = {
            configurable: true,
            get() {
                return originnalMethod.bind(this);
            }
        };
        return adjDescriptor;
    }
}