namespace App{
    type optionsType ={
        keyPath?: string;
        autoIncrement?:boolean;
    }
    export class DBhandler {
        name: string;
        version: number;
        store: string;
        options?: optionsType;
        static instance: DBhandler;

        private constructor (name: string, version: number, store: string, options?: optionsType) {
        this.name = name;
        this.version = version;
        this.store = store;
        this.options = options;
    }
        static getInstance(name: string, version: number, store: string, options?: optionsType) {
            if(this.instance){
                return this.instance;
            }
            return new DBhandler(name,version,store,options);
        }

        successCallback(n: Event, resolve: Function){
             const target = <IDBRequest>n.target;
            resolve((target.result))
        }




        /**
         * Connect to IndexedDB and creates an
         * objectStore based on the constructor details
         * @returns {Promise}
         */
        connect() {
            return new Promise<IDBDatabase>((resolve, reject) => {
                const req = indexedDB.open(this.name, this.version);
                req.onsuccess = () => {
                    //console.log("openDb DONE",req.result);
                    resolve(req.result)
                };
                req.onerror = (evt: Event) => {
                    console.error("openDb:", evt.target);
                    reject(evt)
                };

                req.onupgradeneeded = (evt:IDBVersionChangeEvent) => {
                    this.createObjectStore(evt, this.store, this.options).then((res) => {
                        resolve(res)
                    }).catch((err: any) => {
                        reject(err)
                    });

                };
            })

        }

        /**
         * Checks if a specific objectStore exists.
         * @param db
         * @param objectName
         * @returns {boolean}
         */
        objectStoreExists(db: IDBDatabase, objectName: string):boolean {
            return db.objectStoreNames.contains(objectName)
        }

        /**
         * Creates an objectStore
         * and returns the db to be used with other methods
         * @param e
         * @param objectName
         * @param options
         * @returns {Promise}
         */
        createObjectStore(e: IDBVersionChangeEvent, objectName: string, options?: optionsType) {
            return new Promise<IDBDatabase>((resolve, reject) => {
                const db = (<IDBOpenDBRequest> e.target!).result;
                if (!this.objectStoreExists(db, objectName)) {
                    options ? db.createObjectStore(objectName, options) : db.createObjectStore(objectName);
                }
                const transaction = (<IDBOpenDBRequest>e.target!).transaction!;
                transaction.oncomplete = (event: Event) => {
                    console.log('line 65', event);
                    // Now store is available to be populated
                    resolve(db)
                };
                transaction.onerror = (err: any) => reject(err)

            })


        }
    /**
        * Adds a record of data to the specified objectStore.
    * @param item
    * @param objectStore
    * @returns {Promise<void>}
*/
    async addRecord(item: object, objectStore: string) {
        this.connect().then((db) => {

            const tx = db.transaction([objectStore], 'readwrite');
            const store = tx.objectStore(objectStore);
            const req = store.add(item);
            req.onsuccess = (x: Event) => x.type
        });

    }

    /**
    * Gets a record based on the provided key
    * returns the whole value of the key
    * @param key
    * @param objectStore
    * @returns {Promise}
*/
    getRecord<T>(key: string, objectStore: string) {
        return new Promise<T>((resolve, reject) => {
            this.connect().then((db) => {
                if (this.objectStoreExists(db, objectStore)) {
                    const tx = db.transaction([objectStore], 'readonly');
                    const store = tx.objectStore(objectStore);
                    const req = store.get(key);
                    req.onsuccess = (n:Event) => this.successCallback(n,resolve);
                    req.onerror = (err: any) => reject(err);
                } else {
                    reject(`No store found as "${objectStore}"`)
                }
            }).catch((err) => {
                reject(`connection failed. ${err}`)
            });
        })
    }

    /**
    * updates a record.
    * returns the keyPath of the record
    * @param item
    * @param objectStore
    * @returns {Promise}
*/

    updateRecord(item: object, objectStore: string) {
        return new Promise((resolve, reject) => {
            this.connect().then((db) => {
                if (this.objectStoreExists(db, objectStore)) {
                    const tx = db.transaction([objectStore], 'readwrite');
                    const store = tx.objectStore(objectStore);
                    const req = store.put(item);
                    req.onsuccess = (n: Event) => this.successCallback(n, resolve);
                    req.onerror = (err: any) => reject(err);
                } else {
                    reject(`No store found as "${objectStore}"`)
                }
            }).catch((err) => {
                reject(`connection failed. ${err}`)
            });
        })

    }

    /**
    * deletes a record.
    * returns undefined
    * @param key
    * @param objectStore
    * @returns {Promise}
*/

    deleteRecord(key: number, objectStore: string) {
        return new Promise<IDBRequest>((resolve, reject) => {
            this.connect().then((db) => {
                if (this.objectStoreExists(db, objectStore)) {
                    const tx = db.transaction([objectStore], 'readwrite');
                    const store = tx.objectStore(objectStore);
                    const req = store.delete(key);
                    req.onsuccess = (n: Event) => resolve(<IDBRequest>n.target);
                    req.onerror = (err: any) => reject(err);
                } else {
                    reject(`No store found as "${objectStore}"`)
                }
            }).catch((err) => {
                reject(`connection failed. ${err}`)
            });

        })

    }

    /**
    * gets all the records in an objectStore.
    * Returns an array of items
    * @param objectStore
    * @returns {Promise}
*/
    async getAll<T extends object>(objectStore: string) {
        return new Promise<T>((resolve, reject) => {
            this.connect().then((db) => {
                if (this.objectStoreExists(db, objectStore)) {
                    const tx = (db).transaction([objectStore], 'readonly');
                    const store = tx.objectStore(objectStore);
                    const req = store.getAll();
                    req.onsuccess = (n: Event) => this.successCallback(n, resolve);
                    req.onerror = (err: any) => reject(err);
                } else {
                    reject(`No store found as "${objectStore}"`)
                }
            }).catch((err:any) => {
                reject(`connection failed. ${err}`)
            });
        })
    }

    /**
    * deletes an objectStore
    * @param objectStore
    * @returns {Promise<void>}
*/
    async deleteObjectStore(objectStore: string) {
        return new Promise((resolve, reject) => {
            this.connect().then((db: any) => {
                db.deleteObjectStore(objectStore);
                resolve()
            }).catch(err => reject(err))
        })

    }
    }
    export const db = DBhandler.getInstance('ts',1,'myStore', {keyPath:'id'});
}

