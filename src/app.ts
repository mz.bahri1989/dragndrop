/// <reference path="types.ts"/>
/// <reference path="utils.ts"/>
/// <reference path="decorators/binder.ts"/>
/// <reference path="db/dbHandler.ts"/>
/// <reference path="state/StateManager.ts"/>
/// <reference path="components/BaseComponent.ts"/>
/// <reference path="components/project.ts"/>
/// <reference path="components/formRenderer.ts"/>
/// <reference path="components/projectsListRenderer.ts"/>


namespace App {
    new App.FormRenderer();

    new App.ProjectsListRenderer('active');
    new App.ProjectsListRenderer('finished');

}
