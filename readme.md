A simple project to practice typescript.   
You can create a list of active projects(todo list) and then add them to the finished list via drag and drop.   
The list is saved to IndexedDB.   
To run the project, run `tsc -w` then `npm run start`.